#
# Copyright (c) <2017> <Filippos A. Grapsas> public001@grapsas.com
# MIT License
#
# Crated at 07/02/2019
#

ARG BASE_IMAGE
FROM $BASE_IMAGE

ARG WWW_UID
ARG WWW_GID


# ### Main
ADD files/root/main.sh /root

### MariaDB
RUN mkdir -p /var/lib/mysql-datadir-host/; \
    chown -R mysql:mysql /var/lib/mysql-datadir-host/; \
    mkdir -p /var/run/mysqld; \
    chown -R mysql:mysql /var/run/mysqld/

# ### Apache2
RUN sed -i "s/www-data:x:33:33/www-data:x:$WWW_UID:$WWW_GID/g" /etc/passwd
ADD files/etc/apache2/conf-enabled/ubuntu-16.04-dev.conf /etc/apache2/conf-enabled
RUN mkdir -p /etc/apache2/vhosts
RUN mkdir -p /srv/www/vhosts; \
    chown -R www-data:www-data /srv/www/vhosts

RUN /usr/sbin/a2enmod rewrite expires proxy proxy_http headers

RUN mkdir -p /var/www/.composer/; \
    mkdir -p /var/www/.npm/; \
    mkdir -p /var/www/.config/; \
    mkdir -p /var/www/.cache/; \
    mkdir -p /var/www/.local/; \
    chown -R www-data:www-data /var/www/.composer/; \
    chown -R www-data:www-data /var/www/.npm/; \
    chown -R www-data:www-data /var/www/.config/; \
    chown -R www-data:www-data /var/www/.cache/; \
    chown -R www-data:www-data /var/www/.local/
