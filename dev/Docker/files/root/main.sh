### Apache2
/usr/sbin/service apache2 start

# ### MySQL
if [ ! -f /var/lib/mysql-datadir-host/ibdata1 ]; then
    /usr/bin/echo "MariaDB is not installed. Installing..."
    /usr/sbin/mysqld --initialize-insecure --datadir=/var/lib/mysql-datadir-host --user=mysql
    /usr/bin/echo "Starting mysqld_safe..."
    /usr/bin/mysqld_safe --datadir=/var/lib/mysql-datadir-host --user=mysql  >/dev/null 2>&1 &

    /usr/bin/echo "Checking connection..."
    while ! /usr/bin/mysql -u root --execute="quit"; do
        /usr/bin/echo "MariaDB is not running. Wait"
        sleep 1
    done
    /usr/bin/echo "MariaDB is running"

    /usr/bin/mysql -u root --execute="ALTER USER 'root'@'localhost' IDENTIFIED WITH mysql_native_password BY 'secret';";
fi

/usr/bin/echo "Starting mysqld_safe..."
/usr/bin/mysqld_safe --datadir=/var/lib/mysql-datadir-host --user=mysql  >/dev/null 2>&1 &

### Docker ###
/bin/bash
